package hudson.plugins.viewVCff;

import hudson.Extension;
import hudson.model.Descriptor;
import hudson.scm.RepositoryBrowser;
import hudson.scm.SubversionChangeLogSet.LogEntry;
import hudson.scm.SubversionChangeLogSet.Path;
import hudson.scm.SubversionRepositoryBrowser;
import net.sf.json.JSONObject;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.DataBoundConstructor;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * {@link SubversionRepositoryBrowser} that produces links to the ViewVCff Web Client for SVN
 *
 * @author Sebastian Gerhards and Thorsten Glaser; based on ViewVC plugin by Mike Salnikov, based on Polarion plug-in by Jonny Wray
 */
public class ViewVCffRepositoryBrowser extends SubversionRepositoryBrowser {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String CHANGE_SET_FORMAT = "/scm/viewvc.php?view=rev&root=%s&revision=%d";
	private static final String DIFF_FORMAT = "/scm/viewvc.php%s?root=%s&r1=%d&r2=%d&diff_format=h";
	private static final String FILE_FORMAT = "/scm/viewvc.php%s?root=%s&view=markup";

	
	public final URL url;
	private final String location;

    @DataBoundConstructor
    public ViewVCffRepositoryBrowser(URL url, String location) throws MalformedURLException {
		this.url = normalizeToEndWithSlash(url);
		this.location = location;
	}

    public String getLocation() {
        if(location==null)  return "/";
        return location;
    }

    @Override
    public URL getDiffLink(Path path) throws IOException {
		return new URL(url, String.format(DIFF_FORMAT, path.getValue(), getLocation(), path.getLogEntry().getRevision() - 1, path.getLogEntry().getRevision()));
    }

    @Override
    public URL getFileLink(Path path) throws IOException {
    	return new URL(url, String.format(FILE_FORMAT, path.getValue(), getLocation()));
    }

    @Override
    public URL getChangeSetLink(LogEntry changeSet) throws IOException {
    	return new URL(url, String.format(CHANGE_SET_FORMAT, getLocation(), changeSet.getRevision()));
    }

    @Extension
    public static final class DescriptorImpl extends Descriptor<RepositoryBrowser<?>> {
        public DescriptorImpl() {
            super(ViewVCffRepositoryBrowser.class);
        }

        public String getDisplayName() {
            return "ViewVCff";
        }

        @Override
        public ViewVCffRepositoryBrowser newInstance(StaplerRequest req, JSONObject formData) throws FormException {
		   return req.bindParameters(ViewVCffRepositoryBrowser.class, "viewVCff.");
        }
    }
}
